# Simple Portfolio

### Development setup
1. Clone
2. Restore plugins and themes using [Composer](https://getcomposer.org/).

```bash
composer install
```

3. Use `data/simpleportfolio.sql` to restore the WordPress database.
