<?php

/**
 * Plugin Name: Custom API
 * Plugin URI: http://www.simpleportfolio.com
 * Description: The very first plugin that I have ever created.
 * Version: 1.0.0
 * Author: Daniel Petrov
 * Author URI: http://www.simpleportfolio.com
 */

class CompanyProfile
{
    function __construct($company)
    {
        $this->exchange = $company->exchange;
        $this->website = $company->website;
        $this->name = $company->companyName;
        $this->sector = $company->sector;
        $this->industry = $company->industry;
        $this->ceo = $company->ceo;
        $this->description = $company->description;
        $this->price = round($company->price, 2);
        $this->dividend = round($company->lastDiv, 2);
        $this->dividendPercentage = round(($this->dividend / $this->price) * 100, 2);
        $this->marketCap = $company->mktCap + 0;
        $this->marketCapHumanized = humanizeNumber($company->mktCap);
        $this->beta = round($company->beta, 2);
        $this->betaHumanized = humanizeBeta($company->beta);
    }
}

function sp_company_profile(WP_REST_Request $request)
{
    $ticker = $request->get_param('ticker');

    if (!$ticker) {
        return new WP_Error(400, 'Ticker missing.');
    }

    // cache for 30 minutes
    $company = getCompanyData('company_profile_cache', $ticker, 'fetchCompanyProfile', 1800);

    if (!$company) {
        return new WP_Error(404, "Ticker '$ticker' not found.");
    }

    return new CompanyProfile($company);
}

$exchanges = [
    'any' => 'Any',
    'NYSE' => 'New York Stock Exchange',
    'NASDAQ' => 'Nasdaq Stock Exchange',
    'EURONEXT' => 'Euronext N.V.',
    'AMEX' => 'NYSE American',
    'TSX' => 'Toronto Stock Exchange',
];

function sp_search_company(WP_REST_Request $request)
{
    global $exchanges;
    $query = $request->get_param('q');

    if (!$query) {
        return new WP_Error(400, 'Query is missing.');
    }

    $exchange = $request->get_param('exchange');

    $url = "https://financialmodelingprep.com/api/v3/search?query=$query&limit=10&apikey=20739c15843c158cfe7158b95c0f5ddc";
    if (!array_key_exists($exchange, $exchanges)) {
        $exchange = 'any';
    } elseif ($exchange != 'any') {
        $url .= "&exchange=$exchange";
    }

    $results = fetch($url);

    return $results;
}

add_action('rest_api_init', function () {
    register_rest_route('sp/v1', '/company/(?P<ticker>[A-Z]{1,5})', [
        'methods' => 'GET',
        'callback' => 'sp_company_profile'
    ]);
    register_rest_route('sp/v1', '/search', [
        'methods' => 'GET',
        'callback' => 'sp_search_company'
    ]);
});
