<?php

// [sp_portfolio_table]
function sp_portfolio_table_handler($atts, $content, $tag)
{
    $user_id = get_current_user_id();
    global $wpdb;
    $holdings = $wpdb->get_results("SELECT * FROM holdings WHERE `user_id` = $user_id ORDER BY ticker ASC");
    $is_empty = empty($holdings);
    if ($is_empty) {
        return '<div class="text-muted text-center p-16">It will show here... but right now it is empty :(</div>';
    }

    $tickers = implode(',', array_column($holdings, 'ticker'));
    $url_info = "https://financialmodelingprep.com/api/v3/quote/$tickers?apikey=20739c15843c158cfe7158b95c0f5ddc";
    $holdings_assoc = [];
    foreach ($holdings as $holding) {
        $holdings_assoc[$holding->ticker] = $holding;
    }
    $json = fetch($url_info);

    $portfolio_market_value = 0;
    foreach ($json as $ticker) {
        $quantity = $holdings_assoc[$ticker->symbol]->quantity;
        $market_value = $ticker->price * $quantity;
        $portfolio_market_value += $market_value;
    }

    ob_start(); ?>

    <h3 class="strike-title"><span><?= count($holdings) ?> holdings › $<?= round($portfolio_market_value, 0) ?></span></h3>
    <div style="overflow-x: auto;">
        <table id="tablePortfolio">
            <thead>
                <tr>
                    <th data-type="link">Name</th>
                    <th data-type="number">Shares</th>
                    <th data-type="money">Cost basis</th>
                    <th data-type="money">Total cost</th>
                    <th data-type="money-percentage">Market price</th>
                    <th data-type="money">Market value</th>
                    <th data-type="money-percentage">Unrealised</th>
                    <th data-type="money">Earnings</th>
                    <th data-type="number">P/E ratio</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($json as $ticker) : ?>
                    <?php

                    $quantity = $holdings_assoc[$ticker->symbol]->quantity;
                    $cost_basis = $holdings_assoc[$ticker->symbol]->price;
                    $total_cost = $quantity * $cost_basis;
                    $market_value = $ticker->price * $quantity;
                    $unrealised = $market_value - $total_cost;
                    $unrealised_percent = $unrealised / $total_cost;

                    ?>
                    <tr>
                        <td><a href="company/?ticker=<?= $ticker->symbol ?>"><?= $ticker->name ?> (<?= $ticker->symbol ?>)</a></td>
                        <td><?= $quantity ?></td>
                        <td>$<?= number_format($cost_basis, 2) ?></td>
                        <td>$<?= number_format($total_cost, 2) ?></td>
                        <td>
                            $<?= number_format($ticker->price, 2) ?>
                            <span class="<?= $ticker->changesPercentage > 0 ? 'positive' : 'negative' ?>" style="font-size: 0.8em;"><?= number_format($ticker->changesPercentage, 1) ?>%</span>
                        </td>
                        <td>$<?= number_format($market_value, 2) ?></td>
                        <td class="<?= $unrealised > 0 ? 'positive' : 'negative' ?>">$<?= number_format($unrealised, 2) ?> (<?= humanizePercentage($unrealised_percent) ?>)</td>
                        <td>$<?= number_format($ticker->eps, 2) ?></td>
                        <td><?= number_format($ticker->pe, 1) ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <button class="pmpro_btn" id="buttonExportCsv">Save as CSV</button>
<?php
    return ob_get_clean();
}

// [sp_contact_info email="info@simpleportfolio.com" address="42, Main Street, Valletta, Malta"]
function sp_contact_info_handler($atts, $content, $tag)
{
    extract(shortcode_atts([
        'email' => 'info@simpleportfolio.com',
        'address' => '42, Main Street, Valletta, Malta'
    ], $atts));

    return "<p>Email: <strong><a href=\"mailto:$email\">$email</a></strong></p>
        <p>Address: <strong>$address</strong></p>
        <p><em>Or use the live chat in the bottom right to say ‘Hi’.</em></p>";
}

// [sp_idea]This form will add to your existing holdings too![/sp_idea]
function sp_idea_handler($atts, $content, $tag)
{
    return "<div class=\"mb-16 text-center text-muted\">💡 $content</div>";
}

// [sp_company_search_form]
function sp_company_search_form_handler($atts, $content, $tag)
{
    $company = $_GET['company'];
    $exchange = $_GET['exchange'];

    ob_start(); ?>

    <div class="ashe-widget widget_search" style="max-width: 480px; margin: 16px auto;">
        <form role="search" method="get" id="searchform" class="clear-fix" action="<?= home_url('company-search') ?>">
            <input type="search" name="company" id="company" placeholder="Search..." value="<?= $company ?>" style="margin-bottom: 16px;"><i class="fa fa-search"></i>

            <label for="exchange" class="text-muted">Filter by stock exchange (optional)</label>
            <select name="exchange" id="exchange">
                <?php global $exchanges; ?>
                <?php foreach ($exchanges as $key => $value) : ?>
                    <?php if ($exchange == $key) : ?>
                        <option selected value="<?= $key ?>"><?= $value ?></option>
                    <?php else : ?>
                        <option value="<?= $key ?>"><?= $value ?></option>
                    <?php endif; ?>
                <?php endforeach; ?>
            </select>
            <input type="submit" id="searchsubmit" value="st">
        </form>
    </div>

<?php
    return ob_get_clean();
}

// [sp_company_search_result]
function sp_company_search_result_handler($atts, $content, $tag)
{
    $company = $_GET['company'];
    $exchange = $_GET['exchange'];
    $endpoint = get_site_url() . "/wp-json/sp/v1/search?q=$company&exchange=$exchange";
    $results = fetch($endpoint);
    if (!$results) $results = [];

    ob_start() ?>

    <?php if (empty($results)) : ?>
        <p class="text-center text-muted">Seems like no companies matched your search query. 😭</p>
    <?php else : ?>
        <?php foreach ($results as $ticker) : ?>
            <div class="company-result">
                <h3>
                    <a href="<?= home_url("/company?ticker=$ticker->symbol") ?>"><?= $ticker->name ?></a>
                </h3>
                <div>
                    Exchange: <strong><?= $ticker->exchangeShortName ?></strong> Currency: <strong><?= $ticker->currency ?></strong>
                </div>
                <hr style="max-width: 100px; margin: 32px auto;">
            </div>
        <?php endforeach; ?>
    <?php endif; ?>

<?php
    return ob_get_clean();
}

// [sp_company_button ticker="AAPL"]
function sp_company_button_handler($atts, $content, $tag)
{
    extract(shortcode_atts([
        'ticker' => 'SPY'
    ], $atts));

    $user_id = get_current_user_id();
    global $wpdb;
    $count = 0 + $wpdb->get_var("SELECT COUNT(*) FROM holdings WHERE user_id = $user_id AND ticker = '$ticker'");

    if ($count > 0) {
        $url = home_url("/company?ticker=$ticker");
        return "<div class=\"text-center\">You own <a href=\"$url\">$ticker</a></div>";
    }

    $url = home_url("/new-holding");
    return "<div class=\"wp-block-button aligncenter is-style-outline\">
        <a class=\"wp-block-button__link\" href=\"$url\">Add $ticker to Portfolio</a>
    </div>";
}

// [sp_company_followers ticker="AAPL"]
function sp_company_followers_handler($atts, $content, $tag)
{
    extract(shortcode_atts([
        'ticker' => 'SPY'
    ], $atts));

    global $wpdb;
    $count = 0 + $wpdb->get_var("SELECT COUNT(*) FROM holdings WHERE ticker = '$ticker'");

    return "<div class=\"text-center text-muted\" style=\"margin-bottom: 16px\">($count followers)</div>";
}

function register_shortcodes()
{
    add_shortcode('sp_portfolio_table', 'sp_portfolio_table_handler');
    add_shortcode('sp_contact_info', 'sp_contact_info_handler');
    add_shortcode('sp_idea', 'sp_idea_handler');
    add_shortcode('sp_company_search_form', 'sp_company_search_form_handler');
    add_shortcode('sp_company_search_result', 'sp_company_search_result_handler');
    add_shortcode('sp_company_button', 'sp_company_button_handler');
    add_shortcode('sp_company_followers', 'sp_company_followers_handler');
}

add_action('init', 'register_shortcodes');
