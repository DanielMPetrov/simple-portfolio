<?php /* Template Name: Company */ ?>

<?php

$ticker = $_GET['ticker'];
$endpoint = get_site_url() . '/wp-json/sp/v1/company/' . $ticker;
$company = fetch($endpoint);
if (!$company) {
    // not found
    get_template_part('404');
    die;
}

// cache financials for 14 days
$income_statements = getCompanyData('company_income_cache', $ticker, 'fetchCompanyIncome', 1209600);
$balance_sheets = getCompanyData('company_balance_cache', $ticker, 'fetchCompanyBalance', 1209600);
$cash_flows = getCompanyData('company_cash_cache', $ticker, 'fetchCompanyCash', 1209600);

$fiscal_year_end = date('F', strtotime($income_statements->date[0]));

?>

<?php get_header(); ?>

<div class="main-content clear-fix<?php echo esc_attr(ashe_options('general_content_width')) === 'boxed' ? ' boxed-wrapper' : ''; ?>" data-sidebar-sticky="<?php echo esc_attr(ashe_options('general_sidebar_sticky')); ?>">

    <div class="text-center">

        <p style="margin: 0; color: #777;"><?= $company->exchange ?> - <?= $ticker ?></p>
        <h1>
            <a href="<?= $company->website ?>" target="_blank"><?= $company->name ?></a>
        </h1>
        <p><?= $company->sector ?> &#8250; <?= $company->industry ?></p>
        <p>Chief Executive Officer - <?= $company->ceo ?></p>
        <p style="max-width: 720px; margin: 0 auto;"><?= $company->description ?></p>
        <div class="metrics">
            <div class="metric">
                <div class="metric__name">Price</div>
                <div class="metric__value">$<?= $company->price ?></div>
            </div>
            <div class="metric">
                <div class="metric__name">Dividend (%)</div>
                <div class="metric__value">
                    $<?= $company->dividend ?> (<?= $company->dividendPercentage ?>%)
                </div>
            </div>
            <div class="metric">
                <div class="metric__name">Market Cap</div>
                <div class="metric__value">$<?= $company->marketCapHumanized ?></div>
            </div>
            <div class="metric">
                <div class="metric__name">Beta (Volatility)</div>
                <div class="metric__value"><?= $company->beta ?> (<?= $company->betaHumanized ?>)</div>
            </div>
        </div>

        <hr>

        <?php if (pmpro_hasMembershipLevel(['Premium', 'Pro'])) : ?>
            <h2 class="p-16">Financials</h2>
            <p class="text-muted"><em>Fiscal year ends in <?= $fiscal_year_end ?></em></p>

            <h3 class="collapsible">Income Statement</h3>
            <div class="content">
                <canvas id="incomeStatementChart" style="width: 100% !important; height: 640px !important;"></canvas>
                <div style="overflow-x: auto;">
                    <table id="tableIncomeStatement" style="min-width: 100%">
                        <?php foreach ($income_statements as $key => $values) : ?>
                            <?php if (in_array($key, ['Revenue Growth'])) continue; ?>
                            <tr>
                                <th><?= $key ?></th>
                                <?php foreach ($values as $value) : ?>
                                    <?php if (in_array($key, ['Revenue Growth', 'Gross Margin', 'EBITDA Margin', 'EBIT Margin', 'Profit Margin', 'Free Cash Flow margin', 'Earnings Before Tax Margin', 'Net Profit Margin'])) : ?>
                                        <td><?= humanizePercentage($value) ?></td>
                                    <?php elseif (in_array($key, ['EPS', 'EPS Diluted', 'Dividend per Share'])) : ?>
                                        <td><?= is_numeric($value) ? number_format($value, 2) : '-' ?></td>
                                    <?php elseif ($key === 'date') : ?>
                                        <th><?= date('Y', strtotime($value)) ?></th>
                                    <?php else : ?>
                                        <td><?= humanizeNumber($value) ?></td>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>

            <h3 class="collapsible">Balance Sheet</h3>
            <div class="content">
                <canvas id="balanceSheetChart" style="width: 100% !important; height: 640px !important;"></canvas>
                <div style="overflow-x: auto;">
                    <table id="tableBalanceSheet" style="min-width: 100%">
                        <?php foreach ($balance_sheets as $key => $values) : ?>
                            <tr>
                                <th><?= $key ?></th>
                                <?php foreach ($values as $value) : ?>
                                    <?php if ($key === 'date') : ?>
                                        <th><?= date('Y', strtotime($value)) ?></th>
                                    <?php else : ?>
                                        <td><?= humanizeNumber($value) ?></td>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>

            <h3 class="collapsible">Cash Flow</h3>
            <div class="content">
                <canvas id="cashFlowChart" style="width: 100% !important; height: 640px !important;"></canvas>
                <div style="overflow-x: auto;">
                    <table id="tableCashFlow" style="min-width: 100%">
                        <?php foreach ($cash_flows as $key => $values) : ?>
                            <?php if (in_array($key, ['Net Cash/Marketcap'])) continue; ?>
                            <tr>
                                <th><?= $key ?></th>
                                <?php foreach ($values as $value) : ?>
                                    <?php if ($key === 'date') : ?>
                                        <th><?= date('Y', strtotime($value)) ?></th>
                                    <?php else : ?>
                                        <td><?= humanizeNumber($value) ?></td>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        <?php else : ?>
            <?= do_shortcode('[sp_idea]Did you know Premium and Pro users have access to company financials?[/sp_idea]'); ?>
            <?= do_shortcode('[pmpro_checkout_button class="pmpro_btn" level="2" text="Buy Premium"]'); ?>
            <?= do_shortcode('[pmpro_checkout_button class="pmpro_btn" level="3" text="Buy Pro"]'); ?>
        <?php endif; ?>
    </div>

</div>

<?php get_footer(); ?>