<?php
// Exit if accessed directly
if (!defined('ABSPATH')) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if (!function_exists('chld_thm_cfg_locale_css')) :
    function chld_thm_cfg_locale_css($uri)
    {
        if (empty($uri) && is_rtl() && file_exists(get_template_directory() . '/rtl.css'))
            $uri = get_template_directory_uri() . '/rtl.css';
        return $uri;
    }
endif;
add_filter('locale_stylesheet_uri', 'chld_thm_cfg_locale_css');

if (!function_exists('chld_thm_cfg_parent_css')) :
    function chld_thm_cfg_parent_css()
    {
        wp_enqueue_style('chld_thm_cfg_parent', trailingslashit(get_template_directory_uri()) . 'style.css', array());
    }
endif;
add_action('wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10);

// END ENQUEUE PARENT ACTION

function sp_is_valid_ticker($ticker)
{
    $url = "https://financialmodelingprep.com/api/v3/company/profile/$ticker?apikey=20739c15843c158cfe7158b95c0f5ddc";
    $company = fetch($url);
    return !empty((array) $company);
}

// https://wpforms.com/developers/wpforms_process/
function wpf_dev_process($fields, $entry, $form_data)
{
    global $wpdb;

    if (absint($form_data['id']) === 111) {
        $ticker = strtoupper($fields[1]['value']);
        $quantity = $fields[2]['value'];
        $price = $fields[3]['value'];

        if (!sp_is_valid_ticker($ticker)) {
            wpforms()->process->errors[$form_data['id']]['header'] = esc_html__("'$ticker' is not a valid company ticker symbol.", 'plugin-domain');
            return;
        }

        $holding = $wpdb->get_row(
            $wpdb->prepare("SELECT quantity, price FROM holdings WHERE `user_id`=%d AND ticker=%s", get_current_user_id(), $ticker)
        );

        if (is_null($holding)) {
            $result = $wpdb->insert(
                'holdings',
                array('ticker' => $ticker, 'quantity' => $quantity, 'price' => $price, 'user_id' => get_current_user_id()),
                array('%s', '%d', '%d', '%d')
            );
        } else {
            $total_shares = $holding->quantity + $quantity;
            $total_worth = $holding->quantity * $holding->price + $quantity * $price;
            $new_cost_basis = $total_worth / $total_shares;
            $result = $wpdb->update(
                'holdings',
                array('quantity' => $total_shares, 'price' => $new_cost_basis),
                array('ticker' => $ticker, 'user_id' => get_current_user_id())
            );
        }

        if (!$result) {
            wpforms()->process->errors[$form_data['id']]['header'] = esc_html__('An error occurred while saving your holding.', 'plugin-domain');
        }
        return;
    }

    return $fields;
}
add_action('wpforms_process', 'wpf_dev_process', 10, 3);

include_once plugin_dir_path(__FILE__) . 'shortcodes.php';

function wp_enqueue_scripts_handler()
{
    // global scripts
    wp_enqueue_script('sp-common-scripts', get_stylesheet_directory_uri() . '/assets/js/sp-common.js', array(), '1.0.0', true);

    if (is_page('portfolio')) {
        wp_enqueue_style('table-portfolio', get_stylesheet_directory_uri() . '/assets/css/table-portfolio.css', array(), '1.0.0');
        wp_enqueue_script('chart-js', 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js', array(), false, true);
        wp_enqueue_script('portfolio-pie', get_stylesheet_directory_uri() . '/assets/js/portfolio-pie.js', array(), '1.0.0', true);
        wp_enqueue_script('table-csv-export', get_stylesheet_directory_uri() . '/assets/js/table-csv-export.js', array(), '1.0.0', true);
        wp_enqueue_script('table-sort', get_stylesheet_directory_uri() . '/assets/js/table-sort.js', array(), '1.0.0', true);
    }

    if (is_page('company') && pmpro_hasMembershipLevel(['Premium', 'Pro'])) {
        wp_enqueue_script('chart-js', 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js', array(), false, true);
        wp_enqueue_script('finance-charts-common', get_stylesheet_directory_uri() . '/assets/js/finance-charts-common.js', array(), '1.0.0', true);
        wp_enqueue_script('income-bar-chart', get_stylesheet_directory_uri() . '/assets/js/income-bar-chart.js', array(), '1.0.0', true);
        wp_enqueue_script('balance-bar-chart', get_stylesheet_directory_uri() . '/assets/js/balance-bar-chart.js', array(), '1.0.0', true);
        wp_enqueue_script('cash-bar-chart', get_stylesheet_directory_uri() . '/assets/js/cash-bar-chart.js', array(), '1.0.0', true);
    }
}
add_action('wp_enqueue_scripts', 'wp_enqueue_scripts_handler');

function sp_dd($value)
{
    echo '<pre>';
    var_dump($value);
    echo '</pre>';
    die;
}

function getCompanyData($table, $ticker, $fetch, $seconds)
{
    global $wpdb;

    $seconds_ago = $wpdb->get_var(
        $wpdb->prepare("SELECT TIME_TO_SEC(TIMEDIFF(NOW(), `timestamp`)) FROM $table WHERE ticker = %s", $ticker)
    );

    if (is_null($seconds_ago)) {
        // first time caching
        $company = $fetch($ticker);
        if ($company) {
            $wpdb->query(
                $wpdb->prepare("INSERT INTO $table (ticker, response) VALUES (%s, %s)", $ticker, json_encode($company))
            );
        }
        return $company;
    }

    if ($seconds_ago > $seconds) {
        $company = $fetch($ticker);
        if ($company) {
            $wpdb->update($table, array('response' => json_encode($company), 'timestamp' => null), array('ticker' => $ticker));
        }
        return $company;
    }

    // retrieve the json from cache
    $response = $wpdb->get_var(
        $wpdb->prepare("SELECT `response` FROM $table WHERE ticker = %s", $ticker)
    );
    return json_decode($response);
}

function fetchCompanyProfile($ticker)
{
    $url_info = "https://financialmodelingprep.com/api/v3/company/profile/$ticker?apikey=20739c15843c158cfe7158b95c0f5ddc";
    $json = fetch($url_info);
    $company = $json->profile;

    if (is_null($company)) {
        // not found
        return false;
    }

    return $company;
}

function fetchCompanyIncome($ticker)
{
    $url_income = "https://financialmodelingprep.com/api/v3/financials/income-statement/$ticker?apikey=20739c15843c158cfe7158b95c0f5ddc";
    $json_income = fetch($url_income, true);
    return transpose($json_income['financials']);
}

function fetchCompanyBalance($ticker)
{
    $url_balance = "https://financialmodelingprep.com/api/v3/financials/balance-sheet-statement/$ticker?apikey=20739c15843c158cfe7158b95c0f5ddc";
    $json_balance = fetch($url_balance, true);
    return transpose($json_balance['financials']);
}

function fetchCompanyCash($ticker)
{
    $url_cash = "https://financialmodelingprep.com/api/v3/financials/cash-flow-statement/$ticker?apikey=20739c15843c158cfe7158b95c0f5ddc";
    $json_cash = fetch($url_cash, true);
    return transpose($json_cash['financials']);
}

function fetch($uri, $assoc = false)
{
    $request = wp_remote_get($uri);
    if (is_wp_error($request) || '200' != wp_remote_retrieve_response_code($request)) {
        return false;
    }
    return json_decode(wp_remote_retrieve_body($request), $assoc);
}

// https://stackoverflow.com/questions/797251/transposing-multidimensional-arrays-in-php
function transpose($arr)
{
    $out = array();
    foreach ($arr as $key => $subarr) {
        foreach ($subarr as $subkey => $subvalue) {
            $out[$subkey][$key] = $subvalue;
        }
    }
    return $out;
}

function humanizeBeta($beta)
{
    if ($beta < 0) return 'Negative';
    if ($beta < 0.50) return 'Very Low';
    if ($beta < 0.80) return 'Low';
    if ($beta < 1.10) return 'Average';
    if ($beta < 1.5) return 'High';
    return 'Very High';
}

// modified from: https://www.php.net/manual/en/function.number-format.php
// humanizeNumber(2300) => '2.3K'
// humanizeNumber(1580000, 2) => '1.58M'
function humanizeNumber($n, $precision = 1)
{
    if (!is_numeric($n)) return '-';

    $trillion = 1000000000000;
    $billion = 1000000000;
    $million = 1000000;
    $thousand = 1000;

    if ($n == 0) return '-';
    if ($n > $trillion or $n < -$trillion) return round(($n / $trillion), $precision) . 'T';
    if ($n > $billion or $n < -$billion) return round(($n / $billion), $precision) . 'B';
    if ($n > $million or $n < -$million) return round(($n / $million), $precision) . 'M';
    if ($n > $thousand or $n < -$thousand) return round(($n / $thousand), $precision) . 'K';
    return number_format($n);
}

function humanizePercentage($n)
{
    if (!is_numeric($n)) return '-';
    if ($n == 0) return '-';
    return number_format($n * 100, 1) . '%';
}
