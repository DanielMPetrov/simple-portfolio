(function () {
    const table = document.getElementById('tableCashFlow');

    function readRow(index) {
        var x = Array.from(table.rows[index].cells).map(x => x.innerText);
        x.shift(); // remove the title
        return x;
    }

    var labels = readRow(0);
    var operating = readRow(3).map(parseHumanized);
    var investing = readRow(4).map(parseHumanized);
    var financing = readRow(11).map(parseHumanized);

    var ctx = document.getElementById('cashFlowChart');
    var data = {
        labels: labels,
        datasets: [
            {
                label: 'Operating',
                backgroundColor: 'rgba(153, 102, 255, 0.7)',
                borderColor: 'rgba(153, 102, 255, 1)',
                data: operating,
            },
            {
                label: 'Investing',
                backgroundColor: 'rgba(54, 162, 235, 0.7)',
                borderColor: 'rgba(54, 162, 235, 1)',
                data: investing,
            },
            {
                label: 'Financing',
                backgroundColor: 'rgba(75, 192, 192, 0.7)',
                borderColor: 'rgba(75, 192, 192, 1)',
                data: financing,
            }
        ]
    };

    var myBarChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            title: {
                display: true,
                text: 'Cash Flow Overview (In Millions of USD)'
            },
            barValueSpacing: 20
        }
    });
})();
