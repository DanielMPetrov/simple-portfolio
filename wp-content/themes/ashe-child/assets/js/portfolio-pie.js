(function () {
    const table = document.getElementById('tablePortfolio');
    var canvas = document.getElementById('portfolioPieChart');

    if (!table) {
        canvas.remove();
        return;
    }

    const body = table.querySelector('tbody');
    const items = [];
    for (var i = 0, row; row = body.rows[i]; i++) {
        var name = row.cells[0].firstElementChild.innerHTML.trim();
        var value = parseFloat(row.cells[5].firstChild.nodeValue.trim().replace(',', '').replace('$', ''));
        items.push({ name, value });
    }

    var data = items.map(item => item.value);
    var labels = items.map(item => item.name);
    var myPieChart = new Chart(canvas, {
        type: 'pie',
        data: {
            datasets: [{
                data: data,
                backgroundColor: poolColors(data.length)
            }],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: labels
        },
        options: {
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var dataset = data.datasets[tooltipItem.datasetIndex];
                        var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                            return previousValue + currentValue;
                        });
                        var currentValue = dataset.data[tooltipItem.index];
                        var percentage = Math.floor(((currentValue / total) * 100) + 0.5);

                        var textarea = document.createElement('textarea');
                        textarea.innerHTML = labels[tooltipItem.index];
                        return percentage + '% ' + textarea.value;
                    }
                }
            }
        }
    });

    function poolColors(a) {
        var pool = [];
        for (i = 0; i < a; i++) {
            pool.push(dynamicColors());
        }
        return pool;
    }

    function dynamicColors() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return 'rgba(' + r + ',' + g + ',' + b + ', 0.8)';
    }
})();
