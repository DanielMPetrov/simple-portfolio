/**
 * Adapted from: https://htmldom.dev/export-a-table-to-csv
 */
(function () {
    function toCsv(table) {
        // Query all rows
        const rows = table.querySelectorAll('tr');
        return [].slice.call(rows)
            .map(function (row) {
                // Query all cells
                const cells = row.querySelectorAll('th,td');
                return [].slice.call(cells)
                    .map(function (cell) {
                        return cell.textContent
                            .trim()
                            .replace(',', '')
                            .replace(/\s\s+/g, ' ');
                    })
                    .join(',');
            })
            .join('\n');
    };

    function download(text, fileName) {
        const link = document.createElement('a');
        link.setAttribute('href', `data:text/csv;charset=utf-8,${encodeURIComponent(text)}`);
        link.setAttribute('download', fileName);

        link.style.display = 'none';
        document.body.appendChild(link);

        link.click();

        document.body.removeChild(link);
    };

    const table = document.getElementById('tablePortfolio');

    if (!table) {
        return;
    }

    const exportBtn = document.getElementById('buttonExportCsv');
    exportBtn.addEventListener('click', function () {
        const csv = toCsv(table);
        download(csv, 'portfolio.csv');
    });
})();
