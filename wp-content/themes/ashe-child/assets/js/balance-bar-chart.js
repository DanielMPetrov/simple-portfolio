(function () {
    const table = document.getElementById('tableBalanceSheet');

    function readRow(index) {
        var x = Array.from(table.rows[index].cells).map(x => x.innerText);
        x.shift(); // remove the title
        return x;
    }

    var labels = readRow(0);
    var assets = readRow(12).map(parseHumanized);
    var liabilities = readRow(22).map(parseHumanized);

    var ctx = document.getElementById('balanceSheetChart');
    var data = {
        labels: labels, // [2019, 2018, 2017, 2016, 2015, 2014, 2013, 2012, 2011, 2010, 2009],
        datasets: [
            {
                label: 'Total Assets',
                backgroundColor: 'rgba(153, 102, 255, 0.7)',
                borderColor: 'rgba(153, 102, 255, 1)',
                data: assets,
            },
            {
                label: 'Total Libilities',
                backgroundColor: 'rgba(54, 162, 235, 0.7)',
                borderColor: 'rgba(54, 162, 235, 1)',
                data: liabilities,
            }
        ]
    };

    var myBarChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            title: {
                display: true,
                text: 'Assets vs Liabilities (In Millions of USD)'
            },
            barValueSpacing: 20
        }
    });
})();
