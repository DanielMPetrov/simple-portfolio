// normalise in USD millions
function parseHumanized(numberHumanized) {
    const parsed = parseFloat(numberHumanized);
    if (numberHumanized.endsWith('K')) {
        return parsed / 1000;
    }
    if (numberHumanized.endsWith('M')) {
        return parsed;
    }
    if (numberHumanized.endsWith('B')) {
        return parsed * 1000;
    }
    if (numberHumanized.endsWith('T')) {
        return parsed * 1000 * 1000;
    }
    return parsed / 1000 / 1000;
}
