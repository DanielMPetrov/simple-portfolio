(function () {
    const table = document.getElementById('tableIncomeStatement');

    function readRow(index) {
        var x = Array.from(table.rows[index].cells).map(x => x.innerText);
        x.shift(); // remove the title
        return x;
    }

    var labels = readRow(0);
    var revenue = readRow(1).map(parseHumanized);
    var netIncome = readRow(13).map(parseHumanized);

    var ctx = document.getElementById('incomeStatementChart');
    var data = {
        labels: labels,
        datasets: [
            {
                label: 'Revenue',
                backgroundColor: 'rgba(153, 102, 255, 0.7)',
                borderColor: 'rgba(153, 102, 255, 1)',
                data: revenue,
            },
            {
                label: 'Net Income',
                backgroundColor: 'rgba(54, 162, 235, 0.7)',
                borderColor: 'rgba(54, 162, 235, 1)',
                data: netIncome,
            }
        ]
    };

    var myBarChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            title: {
                display: true,
                text: 'Revenue vs Net Income (In Millions of USD)'
            },
            barValueSpacing: 20
        }
    });
})();
