var collapsibleElements = document.getElementsByClassName('collapsible');
for (let elemement of collapsibleElements) {
    elemement.addEventListener('click', function () {
        this.classList.toggle('collapsible-active');
        var content = this.nextElementSibling;
        if (content.style.maxHeight) {
            content.style.maxHeight = null;
        } else {
            content.style.maxHeight = content.scrollHeight + 'px';
        }
    });
}
