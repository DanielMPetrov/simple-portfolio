<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'simpleportfolio' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'N*lian/`5it>9?Sa!_wO0>Oi|QZOH(^!:+`_urdaG? Yp!PF-oya^-6?L1!F{/ka' );
define( 'SECURE_AUTH_KEY',  '6wi*|MtEI?<h;x;0#?L/Ej7Iw;0o,sb$gYRyb&(P3L:{|B37y-q0,XV-yA]j1iMH' );
define( 'LOGGED_IN_KEY',    '*$oD>C.u+nq~P>t&je09O(G`8tcVu~!c=gr3;fyIi2E|L;d7:o5PFy$Ys+p`N}ez' );
define( 'NONCE_KEY',        'nQj[uOg}r79G_N~T!IGFoQeJ#?#tBdef_xb[F8TC_wLhS*Y:b]W]7:8U[6f|1eKa' );
define( 'AUTH_SALT',        'qhod,?e1iMPlMsfYi:;eB$S,_oG0p,Wg1xJp?#/zETiY?4Ukjm$o:khn0rBRi/&P' );
define( 'SECURE_AUTH_SALT', 'VV%yL:a*g1`8, WH(pidR)mbQ$/0;lX;(Y$mv7}=Pqn:2k^k6`kk.SUs83JFU&}W' );
define( 'LOGGED_IN_SALT',   'z*aj*a@]rn@f8~oTSAg+h/MI5]FMyk13bs_`(ZM^?!S-YTAR#brRw]&-~0{80xuF' );
define( 'NONCE_SALT',       '}NM;aduNI63mvDsCGQ)@Z1SZEgFGbET_xb;=#Vhxg,txsl8A[0KU2Xc@,1Z@0P .' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
